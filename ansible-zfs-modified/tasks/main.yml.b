---
# This playbook contains common plays that will be run on all nodes.

- include: zfs.yml
  when: zfs_enabled 
  tags: [zfs]
  
